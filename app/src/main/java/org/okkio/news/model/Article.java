package org.okkio.news.model;

import org.okkio.news.util.Api;

import java.util.Date;

public class Article {
    private String mId;
    private String mCategory;
    private Date mDatePublished;
    private String mTitle;
    private String mPreview;
    private String mSummary;
    private String mAuthorName;
    private String mAuthorId;
    private String mImageUrl;
    private String mAuthorImageUrl;
    private String Url;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public String getImageUrl() {
        return Api.getAssetUrl(Api.Asset.TH, getId());
    }

    public String getPreview() {
        return mSummary.replaceAll("\r\n|\r|\n", " ");
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public void setAuthorName(String authorName) {
        mAuthorName = authorName;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public Date getDatePublished() {
        return mDatePublished;
    }

    public void setDatePublished(Date datePublished) {
        mDatePublished = datePublished;
    }

    public String getAuthorImageUrl() {
        return getAuthorId() == null ? null : Api.getAssetUrl(Api.Asset.SI, getAuthorId());
    }

    public String getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(String authorId) {
        mAuthorId = authorId;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
