package org.okkio.news.util;

import android.os.AsyncTask;

import org.okkio.news.model.Article;
import org.okkio.news.model.Category;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class Network {

    /**
     * Download bytes from url
     * @param urlSpec
     * @return
     * @throws IOException
     */
    public static byte[] download(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    /**
     * FetchArticlesTask
     */
    public static class FetchArticlesTask extends AsyncTask<Object, Void, List<Article>> {
        private FetchArticlesTask.AsyncTaskListener listener;

        @Override
        protected List<Article> doInBackground(Object... params) {
            return Api.getByCategories((String) params[0], (Integer) params[1]);
        }

        @Override
        protected void onPostExecute(List<Article> items) {
            super.onPostExecute(items);
            if (listener != null) {
                listener.onAsyncTaskFinished(items);
            }
        }

        public void setListener(FetchArticlesTask.AsyncTaskListener listener) {
            this.listener = listener;
        }

        public interface AsyncTaskListener {
            void onAsyncTaskFinished(List<Article> items);
        }
    }

    /**
     * FetchSourcesTask
     */
    public static class FetchSourcesTask extends AsyncTask<Void, Void, List<Category>> {
        private AsyncTaskListener listener;

        @Override
        protected List<Category> doInBackground(Void... params) {
            return Api.getCategories();
        }

        @Override
        protected void onPostExecute(List<Category> items) {
            super.onPostExecute(items);
            if (listener != null) {
                listener.onAsyncTaskFinished(items);
            }
        }

        public void setListener(AsyncTaskListener listener) {
            this.listener = listener;
        }

        public interface AsyncTaskListener {
            void onAsyncTaskFinished(List<Category> items);
        }
    }
}
