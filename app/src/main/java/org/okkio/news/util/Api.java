package org.okkio.news.util;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.okkio.news.model.Article;
import org.okkio.news.model.Category;
import org.okkio.news.model.Source;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class Api {
    public static final String TAG = "Api";
    private static final String BASE_URL = "http://news.okkio.org/api/";

    public enum Asset {TH, SI}

    /**
     * Get articles by ids
     *
     * @param sources Comma separated string
     * @param page    Page num
     * @return
     */
    public static List<Article> getBySources(String sources, int page) {
        Map<String, String> params = new HashMap<>();
        params.put("count", "50");
        params.put("page", String.valueOf(page));
        params.put("ids", sources);
        return fetchArticles(buildUrl("feed", params));
    }

    /**
     * Get articles by category
     *
     * @param categories Comma separated string
     * @param page       Page num
     * @return
     */
    public static List<Article> getByCategories(String categories, int page) {
        Map<String, String> params = new HashMap<>();
        params.put("count", "50");
        params.put("page", String.valueOf(page));
        params.put("cats", categories);
        return fetchArticles(buildUrl("news", params));
    }

    /**
     * Get categories list with sources
     *
     * @return
     */
    public static List<Category> getCategories() {
        return fetchCategories(buildUrl("categories", null));
    }

    private static List<Article> fetchArticles(String url) {
        List<Article> newsItems = new ArrayList<>();
        try {
            String jsonString = new String(Network.download(url));
            JSONObject json = new JSONObject(jsonString);
            JSONArray articles = json.getJSONArray("articles");
            for (int i = 0; i < articles.length(); i++) {
                newsItems.add(parseArticle(articles.getJSONObject(i)));
            }
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch JSON", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }

        return newsItems;
    }

    private static List<Category> fetchCategories(String url) {
        List<Category> categoryItems = new ArrayList<>();
        try {
            String jsonString = new String(Network.download(url));
            JSONObject json = new JSONObject(jsonString);
            JSONArray categories = json.getJSONArray("categories");
            for (int i = 0; i < categories.length(); i++) {
                categoryItems.add(parseCategory(categories.getJSONObject(i)));
            }
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch JSON", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }

        return categoryItems;
    }

    private static Article parseArticle(JSONObject articleJsonObject) throws JSONException {
        Article item = new Article();
        item.setId(articleJsonObject.getString("id"));
        item.setTitle(articleJsonObject.getString("title"));
        item.setSummary(articleJsonObject.getString("summary"));
        item.setAuthorName(articleJsonObject.getJSONObject("feed").getString("title"));
        item.setCategory(articleJsonObject.getString("category"));
        item.setUrl(articleJsonObject.getString("url"));
        String input = articleJsonObject.getString("date_published");
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(input);
            item.setDatePublished(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            JSONArray icons = articleJsonObject.getJSONObject("feed").getJSONArray("icons");
            item.setAuthorId(icons.getJSONArray(0).getString(3));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    private static Category parseCategory(JSONObject categoryJsonObject) throws JSONException {
        Category item = new Category();
        List<Source> sourceItems = new ArrayList<>();
        item.setName(categoryJsonObject.getString("name"));
        item.setSlug(categoryJsonObject.getString("slug"));
        item.setCode(categoryJsonObject.getString("code"));
        JSONArray sources = categoryJsonObject.getJSONArray("sources");
        for (int i = 0; i < sources.length(); i++) {
            Source source = new Source();
            source.setId(sources.getJSONObject(i).getInt("id"));
            source.setName(sources.getJSONObject(i).getString("name"));
            source.setPopularity(sources.getJSONObject(i).getDouble("popularity"));
            sourceItems.add(source);
        }
        item.setSources(sourceItems);
        return item;
    }

    /**
     * Build url for images
     *
     * @param type
     * @param id
     * @return
     */
    public static String getAssetUrl(Asset type, String id) {
        Map<String, String> params = new HashMap<>();
        params.put("type", type == Asset.SI ? "si" : "tn");
        params.put("id", id);
        return buildUrl("asset", params);
    }

    private static String buildUrl(String method, Map<String, String> params) {
        Uri.Builder builder = Uri.parse(BASE_URL).buildUpon().appendPath(method);
        if (params != null) {
            for (Map.Entry entry : params.entrySet()) {
                builder.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
            }
        }
        return builder.build().toString();
    }
}
