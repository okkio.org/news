package org.okkio.news.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.text.TextUtils;

import org.okkio.news.R;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Helper {
    private static final String SHARED_PREF_NAME = "org.okkio.news.pref";
    private static final String SHARED_PREF_KEY_CATEGORIES = "categories";

    /**
     * Humanize date moment
     *
     * @param date
     * @param context
     * @return
     */
    public static String getTimeAgo(Date date, Context context) {
        long days = (new Date().getTime() - date.getTime()) / 86400000;
        Resources resources = context.getResources();
        if (days == 0) {
            Date now = new Date();
            String text;
            long res = TimeUnit.MILLISECONDS.toHours(now.getTime() - date.getTime());
            if (res == 0) {
                res = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - date.getTime());
                text = resources.getQuantityString(R.plurals.minutes, (int) res, (int) res);
            } else {
                text = resources.getQuantityString(R.plurals.hours, (int) res, (int) res);
            }
            return text;
        } else {
            return days == 1 ? resources.getString(R.string.yesterday) : resources.getQuantityString(R.plurals.days, (int) days, (int) days);
        }
    }

    /**
     * @param context
     * @return Comma separated string
     */
    public static String loadFromPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Set<String> set = sharedPreferences.getStringSet(SHARED_PREF_KEY_CATEGORIES, null);
        if (set != null && !set.isEmpty()) {
            return TextUtils.join(",", set);
        }
        return null;
    }

    /**
     * @param categories Comma separated string
     * @param context
     */
    public static void saveToPreferences(String categories, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Set<String> set = new HashSet<>(Arrays.asList(categories.split(",")));
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(SHARED_PREF_KEY_CATEGORIES, set).apply();
    }
}
