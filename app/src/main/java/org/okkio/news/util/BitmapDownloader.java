package org.okkio.news.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.util.Log;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class BitmapDownloader<T> extends HandlerThread {
    private static final String TAG = "BitmapDownloader";
    private static final int MESSAGE_DOWNLOAD = 0;

    private boolean mHasQuit = false;
    private Handler mRequestHandler;
    private ConcurrentMap<T, String> mRequestMap = new ConcurrentHashMap<>();
    private Handler mResponseHandler;
    private BitmapDownloaderListener<T> mBitmapDownloaderListener;
    private LruCache<String, Bitmap> mMemoryCache;

    public interface BitmapDownloaderListener<T> {
        void onBitmapDownloaded(T target, Bitmap bitmap);
    }

    public void setBitmapDownloaderListener(BitmapDownloaderListener<T> listener) {
        mBitmapDownloaderListener = listener;
    }

    public BitmapDownloader(Handler responseHandler) {
        super(TAG);
        mResponseHandler = responseHandler;

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(@NonNull String key, @NonNull Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    @Override
    protected void onLooperPrepared() {
        mRequestHandler = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                @SuppressWarnings("unchecked")
                T target = (T) msg.obj;
                handleRequest(target);
                return true;
            }
        });
    }

    @Override
    public boolean quit() {
        mHasQuit = true;
        return super.quit();
    }

    public void queueBitmap(T target, String url) {
        if (url == null) {
            mRequestMap.remove(target);
        } else {
            mRequestMap.put(target, url);
            mRequestHandler.obtainMessage(MESSAGE_DOWNLOAD, target).sendToTarget();
        }
    }

    public void clearQueue() {
        mRequestHandler.removeMessages(MESSAGE_DOWNLOAD);
        mRequestMap.clear();
    }

    private void handleRequest(final T target) {
        final String url = mRequestMap.get(target);
        if (url == null) {
            return;
        }

        try {
            Bitmap bitmap = getBitmapFromMemCache(url);
            if (bitmap == null) {
                byte[] bitmapBytes = Network.download(url);
                bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
                addBitmapToMemoryCache(url, bitmap);
            }

            final Bitmap finalBitmap = bitmap;
            mResponseHandler.post(new Runnable() {
                public void run() {
                    if (!Objects.equals(mRequestMap.get(target), url) || mHasQuit) {
                        return;
                    }

                    mRequestMap.remove(target);
                    mBitmapDownloaderListener.onBitmapDownloaded(target, finalBitmap);
                }
            });
        } catch (IOException ioe) {
            Log.e(TAG, "Error downloading image", ioe);
        }
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null && bitmap != null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
}
