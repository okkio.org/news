package org.okkio.news;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.okkio.news.model.Article;
import org.okkio.news.util.BitmapDownloader;
import org.okkio.news.util.Helper;
import org.okkio.news.util.Network;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_SOURCES = 0;

    private List<Article> mArticleList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private BitmapDownloader<ImageView> mBitmapDownloader;
    private SwipeRefreshLayout mSwipeRefresh;
    private AppBarLayout mAppBarLayout;
    private ProgressBar mProgressBar;

    private String mCategories = "te,en";
    private boolean mIsLoading = false;
    private int mPastVisibleItems, mVisibleItemCount, mTotalItemCount, mPage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefresh = findViewById(R.id.swiperefresh);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 0;
                mArticleList = new ArrayList<>();
                mRecyclerViewAdapter.notifyDataSetChanged();
                loadArticles(mPage);
            }
        });

        mAppBarLayout = findViewById(R.id.app_bar_layout);
        mProgressBar = findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAppBarLayout.setExpanded(true);
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, CategoriesActivity.class);
                startActivityForResult(i, REQUEST_CODE_SOURCES);
            }
        });

        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setItemPrefetchEnabled(true);
        mLayoutManager.setInitialPrefetchItemCount(15);
        mRecyclerViewAdapter = new RecyclerViewAdapter();
        mRecyclerView = findViewById(R.id.articles_list);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        // setOnScrollListener is deprecated but setOnScrollChangeListener requires API level 23
        // we have min api 19
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    mVisibleItemCount = mLayoutManager.getChildCount();
                    mTotalItemCount = mLayoutManager.getItemCount();
                    mPastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (mIsLoading) {
                        return;
                    }

                    if ((mVisibleItemCount + mPastVisibleItems) >= mTotalItemCount - 5) {
                        mPage++;
                        loadArticles(mPage);
                    }
                }
            }
        });

        Handler responseHandler = new Handler();
        mBitmapDownloader = new BitmapDownloader<>(responseHandler);
        mBitmapDownloader.setBitmapDownloaderListener(
                new BitmapDownloader.BitmapDownloaderListener<ImageView>() {
                    @Override
                    public void onBitmapDownloaded(ImageView image, Bitmap bitmap) {
                        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                        image.setImageDrawable(drawable);
                    }
                }
        );
        mBitmapDownloader.start();
        mBitmapDownloader.getLooper();

        String prefs = Helper.loadFromPreferences(this);
        mCategories = (prefs != null && !prefs.isEmpty()) ? prefs : mCategories;
        Helper.saveToPreferences(mCategories, this); // save default categories
        loadArticles(mPage);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_SOURCES) {
            mPage = 0;
            String prefs = Helper.loadFromPreferences(this);
            mCategories = (prefs != null && !prefs.isEmpty()) ? prefs : mCategories;
            mArticleList = new ArrayList<>();
            mRecyclerViewAdapter.notifyDataSetChanged();
            loadArticles(mPage);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBitmapDownloader.quit();
        mBitmapDownloader.clearQueue();
    }

    private void loadArticles(int page) {
        mProgressBar.setVisibility(View.VISIBLE);
        mIsLoading = true;
        Network.FetchArticlesTask fetchArticlesTask = new Network.FetchArticlesTask();
        fetchArticlesTask.setListener(new Network.FetchArticlesTask.AsyncTaskListener() {
            @Override
            public void onAsyncTaskFinished(List<Article> items) {
                mIsLoading = false;
                mArticleList.addAll(items);
                mRecyclerViewAdapter.notifyItemInserted(mArticleList.size() - 1);
                mSwipeRefresh.setRefreshing(false);
                mProgressBar.setVisibility(View.GONE);
            }
        });
        fetchArticlesTask.execute(mCategories, page);
    }

    //region Adapter
    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
        Drawable mPlaceholderArticle, mPlaceholderAvatar;

        RecyclerViewAdapter() {
            mPlaceholderArticle = getResources().getDrawable(R.drawable.placeholder_news);
            mPlaceholderAvatar = getResources().getDrawable(R.drawable.placeholder_avatar);
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
            return new ViewHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Article article = mArticleList.get(position);
            holder.mImageView.setImageDrawable(mPlaceholderArticle);
            holder.mAuthorAvatarView.setImageDrawable(mPlaceholderAvatar);
            mBitmapDownloader.queueBitmap(holder.mImageView, article.getImageUrl());
            if (article.getAuthorImageUrl() != null) {
                mBitmapDownloader.queueBitmap(holder.mAuthorAvatarView, article.getAuthorImageUrl());
            }
            holder.mTitleView.setText(article.getTitle());
            holder.mPreviewView.setText(article.getPreview());
            holder.mAuthorView.setText(article.getAuthorName());
            holder.mDateView.setText(Helper.getTimeAgo(article.getDatePublished(), getBaseContext()));
            int resId = getResources().getIdentifier("category_" + article.getCategory(), "string", getPackageName());
            if (resId != 0) {
                holder.mCategoryView.setText(getString(resId));
            }
        }

        @Override
        public int getItemCount() {
            return mArticleList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private TextView mCategoryView;
            private TextView mDateView;
            private TextView mTitleView;
            private TextView mPreviewView;
            private TextView mAuthorView;
            public ImageView mImageView, mAuthorAvatarView;

            public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
                super(inflater.inflate(R.layout.article_item, parent, false));
                itemView.setOnClickListener(this);
                mCategoryView = itemView.findViewById(R.id.category);
                mDateView = itemView.findViewById(R.id.publish_time);
                mAuthorView = itemView.findViewById(R.id.author_name);
                mTitleView = itemView.findViewById(R.id.title);
                mPreviewView = itemView.findViewById(R.id.preview);
                mImageView = itemView.findViewById(R.id.photo);
                mAuthorAvatarView = itemView.findViewById(R.id.author_avatar);
            }

            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                if (pos == RecyclerView.NO_POSITION) {
                    return;
                }
                Article article = mArticleList.get(pos);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(article.getUrl()));
                startActivity(i);
            }
        }
    }
    //endregion
}
