package org.okkio.news;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.okkio.news.model.Category;
import org.okkio.news.util.Helper;
import org.okkio.news.util.Network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity {
    private List<Category> mCategoryList = new ArrayList<>();
    private List<String> mSelected = new ArrayList<>();
    private RecyclerViewAdapter mRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sources);
        RecyclerView recyclerView = findViewById(R.id.sources_list);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        mRecyclerViewAdapter = new RecyclerViewAdapter();
        recyclerView.setAdapter(mRecyclerViewAdapter);

        String prefs = Helper.loadFromPreferences(this);
        if (prefs != null) {
            mSelected.addAll(Arrays.asList(prefs.split(",")));
        }
        loadCategories();
    }

    private void loadCategories() {
        Network.FetchSourcesTask fetchSourcesTask = new Network.FetchSourcesTask();
        fetchSourcesTask.setListener(new Network.FetchSourcesTask.AsyncTaskListener() {
            @Override
            public void onAsyncTaskFinished(List<Category> items) {
                mCategoryList = items;
                mRecyclerViewAdapter.notifyDataSetChanged();
            }
        });
        fetchSourcesTask.execute();
    }

    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
        int mSelectedColor = ContextCompat.getColor(getBaseContext(), R.color.color_accent);
        int mDefaultColor = ContextCompat.getColor(getBaseContext(), R.color.color_primary);

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(CategoriesActivity.this);
            return new RecyclerViewAdapter.ViewHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
            Category category = mCategoryList.get(position);
            int resId = getResources().getIdentifier("category_" + category.getCode(), "string", getPackageName());
            viewHolder.mTitleView.setText(resId == 0 ? category.getName() : getString(resId));
            if (mSelected.contains(category.getCode())) {
                viewHolder.itemView.setBackgroundColor(mSelectedColor);
            } else {
                viewHolder.itemView.setBackgroundColor(mDefaultColor);
            }
        }

        @Override
        public int getItemCount() {
            return mCategoryList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView mTitleView;

            public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
                super(inflater.inflate(R.layout.source_item, parent, false));
                mTitleView = itemView.findViewById(R.id.title);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                if (pos == RecyclerView.NO_POSITION) {
                    return;
                }
                String selectedCode = mCategoryList.get(pos).getCode();
                if (mSelected.contains(selectedCode)) {
                    itemView.setBackgroundColor(mDefaultColor);
                    mSelected.remove(selectedCode);
                } else {
                    itemView.setBackgroundColor(mSelectedColor);
                    mSelected.add(selectedCode);
                }
                Helper.saveToPreferences(TextUtils.join(",", mSelected), getBaseContext());
            }
        }
    }
}
